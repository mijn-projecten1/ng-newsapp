import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { NewsApiService } from '../service/news-api.service';

@Component({
  selector: 'app-sport-news',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sport-news.component.html',
  styleUrl: './sport-news.component.css'
})
export class SportNewsComponent {

  sportNewsResult: any=[];
  constructor(private newsservice: NewsApiService){
    this.newsservice.getSportNews().subscribe((result) =>{
      console.log(result);
      this.sportNewsResult = result.articles;
    })
  }

}
