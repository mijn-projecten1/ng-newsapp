import { Component } from '@angular/core';
import { NewsApiService } from '../service/news-api.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-entertainment',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './entertainment.component.html',
  styleUrl: './entertainment.component.css'
})
export class EntertainmentComponent {

  entertainmentNewsResult: any=[];

  constructor(private newsservice: NewsApiService){
    this.newsservice.getEnterNews().subscribe((result) =>{
      console.log(result);
      this.entertainmentNewsResult = result.articles;
    })
  }

}
