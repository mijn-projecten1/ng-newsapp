import { Component } from '@angular/core';
import { NewsApiService } from '../service/news-api.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-businessnews',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './businessnews.component.html',
  styleUrl: './businessnews.component.css'
})
export class BusinessnewsComponent {

  businessNewsResult:any=[];

  constructor(private newsService:NewsApiService){
    this.newsService.getBusinessNews().subscribe((result)=>{
      console.log(result);
    this.businessNewsResult = result.articles;
    })
  }

}
