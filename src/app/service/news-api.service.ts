import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsApiService {

  constructor(private http: HttpClient) { }

 private readonly topHeadlinesApiUrl = 'https://newsapi.org/v2/top-headlines?country=us&apiKey=bdb8b2fdf5634ee98da1293ca5e9b1a8'
  private readonly businessNewsApiUrl = 'https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=bdb8b2fdf5634ee98da1293ca5e9b1a8'
  private readonly enterNewsApiUrl = 'https://newsapi.org/v2/top-headlines?country=us&category=entertainment&apiKey=bdb8b2fdf5634ee98da1293ca5e9b1a8'
  private readonly healthNewsApiUrl = 'https://newsapi.org/v2/top-headlines?country=us&category=health&apiKey=bdb8b2fdf5634ee98da1293ca5e9b1a8'
  private readonly scienceNewsApiUrl = 'https://newsapi.org/v2/top-headlines?country=us&category=science&apiKey=bdb8b2fdf5634ee98da1293ca5e9b1a8'
  private readonly techNewsApiUrl = 'https://newsapi.org/v2/top-headlines?country=us&category=technology&apiKey=bdb8b2fdf5634ee98da1293ca5e9b1a8'
  private readonly sportNewsApiUrl = 'https://newsapi.org/v2/top-headlines?country=us&category=sport&apiKey=bdb8b2fdf5634ee98da1293ca5e9b1a8'

  topHeadlines(): Observable<any> {
    return this.http.get(this.topHeadlinesApiUrl);
  }

  getBusinessNews(): Observable<any> {
    return this.http.get(this.businessNewsApiUrl);
  }

  getEnterNews(): Observable<any> {
    return this.http.get(this.enterNewsApiUrl);
  }

  getHealthNews(): Observable<any> {
    return this.http.get(this.healthNewsApiUrl);
  }

  getScienceNews(): Observable<any> {
    return this.http.get(this.scienceNewsApiUrl);
  }

  getTechnologyNews(): Observable<any> {
    return this.http.get(this.techNewsApiUrl);
  }

  getSportNews(): Observable<any>{
    return this.http.get(this.sportNewsApiUrl);
  }
}
