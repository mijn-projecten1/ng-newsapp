import { Routes } from '@angular/router';
import { HeadlinesComponent } from './headlines/headlines.component';
import { TechnewsComponent } from './technews/technews.component';
import { BusinessnewsComponent } from './businessnews/businessnews.component';
import { HealthNewsComponent } from './health-news/health-news.component';
import { EntertainmentComponent } from './entertainment/entertainment.component';
import { ScienceNewsComponent } from './science-news/science-news.component';
import { SportNewsComponent } from './sport-news/sport-news.component';

export const routes: Routes = [
    {path: '', component: HeadlinesComponent},
    {path: 'business', component:BusinessnewsComponent},
    {path: 'entertainment', component:EntertainmentComponent},
    {path: 'health', component:HealthNewsComponent},
    {path: 'science', component:ScienceNewsComponent},
    {path: 'technology', component : TechnewsComponent},
    {path: 'sport', component:SportNewsComponent}
];
