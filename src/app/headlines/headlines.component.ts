import { Component } from '@angular/core';
import { NewsApiService } from '../service/news-api.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-headlines',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './headlines.component.html',
  styleUrl: './headlines.component.css'
})
export class HeadlinesComponent {
  topHeadlinesResult: any = [];

  constructor(private newsService: NewsApiService) {
    this.newsService.topHeadlines().subscribe((result) => {
      console.log(result);
      this.topHeadlinesResult = result.articles;
    })
  }

}
