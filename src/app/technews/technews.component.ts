import { Component } from '@angular/core';
import { NewsApiService } from '../service/news-api.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-technews',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './technews.component.html',
  styleUrl: './technews.component.css'
})
export class TechnewsComponent {
  technologyNewsResult: any = [];

  constructor(private newsService: NewsApiService) {
    this.newsService.getTechnologyNews().subscribe((result) => {
      console.log(result);
      this.technologyNewsResult = result.articles;
    })
  }

}
