import { Component } from '@angular/core';
import { NewsApiService } from '../service/news-api.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-health-news',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './health-news.component.html',
  styleUrl: './health-news.component.css'
})
export class HealthNewsComponent {

  healthNewsResult: any=[];

  constructor(private newsService: NewsApiService){
    this.newsService.getHealthNews().subscribe((result) =>
    {
      console.log(result);
      this.healthNewsResult = result.articles;
    })
  }

}
