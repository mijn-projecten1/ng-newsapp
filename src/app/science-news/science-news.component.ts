import { Component } from '@angular/core';
import { NewsApiService } from '../service/news-api.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-science-news',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './science-news.component.html',
  styleUrl: './science-news.component.css'
})
export class ScienceNewsComponent {

  scienceNewsResult: any=[];

  constructor(private newsservice: NewsApiService){
    this.newsservice.getScienceNews().subscribe((result) =>{
      console.log(result);
      this.scienceNewsResult = result.articles;
    })
  }

}
